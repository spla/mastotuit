import getpass
import os
import sys
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import pdb

def get_dbconfig():

    # Load configuration from config file
    config_filepath = "config/db_config.txt"
    feeds_db = get_parameter("feeds_db", config_filepath)
    feeds_db_user = get_parameter("feeds_db_user", config_filepath)
    feeds_url = get_parameter("feeds_url", config_filepath)

    return (config_filepath, feeds_db, feeds_db_user, feeds_url)

def get_parameter( parameter, file_path ):

    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def create_db():

    create_error = ''

    conn = None

    try:

      conn = psycopg2.connect(dbname='postgres',
          user=feeds_db_user, host='',
          password='')

      conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

      cur = conn.cursor()

      print("Creating database " + feeds_db + ". Please wait...")

      cur.execute(sql.SQL("CREATE DATABASE {}").format(
              sql.Identifier(feeds_db))
          )
      print("Database " + feeds_db + " created!")

    except (Exception, psycopg2.DatabaseError) as error:

        create_error = error.pgcode

        sys.stdout.write(f'\n{str(error)}\n')

    finally:

        if conn is not None:

            conn.close()

    return create_error

def check_db_conn():

    try:

        conn = None

        conn = psycopg2.connect(database = feeds_db, user = feeds_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

        os.remove(config_filepath)

        sys.exit('Exiting. Run db-setup again with right parameters')

    if conn is not None:

        print("\n")
        print("mastotuit parameters saved to db-config.txt!")
        print("\n")

def write_parameter( parameter, file_path ):

    if not os.path.exists('config'):
        os.makedirs('config')
    print("Setting up mastotuit parameters...")
    print("\n")
    feeds_db = input("feeds db name: ")
    feeds_db_user = input("feeds db user: ")
    feeds_url = input("enter Mastodon user RSS feeds url: ")

    with open(file_path, "w") as text_file:
        print("feeds_db: {}".format(feeds_db), file=text_file)
        print("feeds_db_user: {}".format(feeds_db_user), file=text_file)
        print("feeds_url: {}".format(feeds_url), file=text_file)

def create_table(db, db_user, table, sql):

  conn = None
  try:

    conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
    cur = conn.cursor()

    print("Creating table.. "+table)
    # Create the table in PostgreSQL database
    cur.execute(sql)

    conn.commit()
    print("Table "+table+" created!")
    print("\n")

  except (Exception, psycopg2.DatabaseError) as error:

    print(error)

  finally:

    if conn is not None:

      conn.close()

#############################################################################################
# main

if __name__ == '__main__':

    config_filepath, feeds_db, feeds_db_user, feeds_url = get_dbconfig()

    create_error = create_db()

    if create_error == '':

        check_db_conn()

    else:

        if create_error == '42P04':

            sys.exit()

        else:

            os.remove(config_filepath)

            sys.exit()

    ############################################################
    # Create needed tables
    ############################################################

    db = feeds_db
    db_user = feeds_db_user
    table = "id"
    sql = f'create table {table} (toot_id bigint PRIMARY KEY, tweet_id bigint)'

    create_table(db, db_user, table, sql)

    print(f'Done!\nNow you can run setup.py\n')
